module Main where

import qualified Data.List as List
import qualified Control.Exception.Base as CEB

dim = [2,3,4]

coordinate :: Int -> String
coordinate 0 = "x"
coordinate 1 = "y"
coordinate 2 = "z"
coordinate 3 = "w"
coordinate 4 = "u"
coordinate 5 = "v"
coordinate _ = undefined

sigma_lattice n = (1.0 / (sqrt (1.0 + fn)) - 1.0) / fn
	where
		fn = fromIntegral n
sigma_euclidean n = ((sqrt (1.0 + fn)) - 1.0) / fn
	where
		fn = fromIntegral n

-- | Indent a list of strings by 1 level.
indent li = map (\x -> "\t" ++ x) li

-- | Calculate all unique combinations of [1,1,...,0,0] where there are
-- m 1's and (n-m) 0's
combinatorial :: Int -> Int -> [[Int]]
combinatorial n m
	| m == 0 = [replicate n (0 :: Int)]
	| m == n = [replicate n (1 :: Int)]
	| otherwise = (map ([0] ++) (combinatorial (n-1) m)) ++
	              (map ([1] ++) (combinatorial (n-1) (m-1)))

--map2 :: (Int -> Int -> String) -> [Int] -> [Int] -> [String]
map2 f _ [] = []
map2 f [] _ = []
map2 f (x:xs) (y:ys) = (f x y) : (map2 f xs ys)


-- |
gen_index :: String -> Int -> String
gen_index array i = array ++ "[" ++ (show i) ++ "]"

-- | Generate a sum
gen_binOp :: String -> (Int -> String) -> Int -> String
gen_binOp op f i = List.intercalate (" " ++ op ++ " ") $ map f [0..i-1]

-- | Generate the name of extrapolate function
gen_extrapolateName :: Int -> String
gen_extrapolateName n = "extrapolate" ++ (show n)

-- | Generate the extrapolate function
gen_extrapolate :: Int -> [String]
gen_extrapolate n = [
	"static inline void",
	(gen_extrapolateName n) ++ "(NoiseOpenSimplex<" ++ (show n) ++ ">::Data& d,",
	"\treal const* const surflets, int const* const perm[" ++ (show n) ++ "],",
	"\t" ++ (concat $ map argInteger [0..n-1]),
	"\t" ++ (List.intercalate ", " $ map argFloat [0..n-1]) ++ ")",
	"{",
	-- Attenuation
	"\treal const atten = 2.0" ++ (concat $ map (\i -> " - pow2(x" ++ (show i) ++ ")") [0..n-1]) ++ ";",
	"\tif (atten <= 0.0) return;",
	-- Compute the index and dereference the surflet
	"\tint const index = " ++ (gen_binOp "^" hashAxis n) ++ ";",
	"\treal const surflet[] = {"
	] ++ (map surfletExtract [0..n-1]) ++
	[
	"\t};",
	-- Compute the dot product
	"\treal const prod[] = {"
	] ++ (map dotComponent [0..n-1]) ++
	[
	"\t};",
	"\treal const dot = " ++ (gen_binOp "+" (gen_index "prod") n) ++ ";",
	"\treal const weight = pow4(atten);",
	"\td.f += weight * dot;",
	"}"
	]
	where
		argInteger i = "integer const xi" ++ (show i) ++ ", "
		argFloat i = "real const x" ++ (show i)
		hashAxis i = "perm[" ++ (show i) ++ "][xi" ++ (show i) ++ " & PERM_MASK]"
		surfletExtract i = "\t\tsurflets[index + " ++ (show i) ++ "],"
		dotComponent i = "\t\t" ++ (gen_index "surflet" i) ++ " * x" ++ (show i) ++ ","

-- Generate all contributors from a given hyperplane
gen_hyperplane :: Int -> Int -> [String]
gen_hyperplane n m
	| m == 0 =
		[
			(gen_extrapolateName n) ++ "(d, surflets_, perm_" ++
			(concat $ map (xli 0) [0..n-1]) ++
			(concat $ map (gen_index ", d.ef") [0..n-1]) ++ ");"
		]

	| m == n = (contrib_variables 1) ++
		[
			(extrapolate_vertex $ replicate n 1)
		]
	| otherwise  = (contrib_variables 0) ++ (contrib_variables 1) ++
		(map extrapolate_vertex $ combinatorial n m)
		where
			xli 0 = gen_index ", d.li"
			xli 1 = gen_index ", lj"
			xef 0 i = "ef" ++ (show m) ++ "_" ++ (show i)
			xef 1 i = "eg" ++ (show m) ++ "_" ++ (show i)

			-- | Generate the ith contribution variables with the given lattice
			-- shift. That is,
			--
			-- ls = 0: real const ef4_2 = d.lf[2] - EUCLIDEAN_4
			-- ls = 1: real const ef4_2 = d.lf[2] - 1 - EUCLIDEAN_4
			contrib_variable ls i = "real const " ++ (xef ls i) ++ " = " ++
				(gen_index "d.ef" i) ++ (if ls == 1 then " - 1" else "") ++
				" - EUCLIDEAN_" ++ (show m) ++ ";"
			contrib_variables ls = map (contrib_variable ls) [0..n-1]

			extrapolate_vertex v = (gen_extrapolateName n) ++ "(d, surflets_, perm_" ++
				(concat $ map2 xli v [0..n-1]) ++
				(concat $ map2 (\x y -> ", " ++ (xef x y)) v [0..n-1]) ++ ");"


			

-- | Generate the series of if's:
-- 
-- if (d.sum_lf < 1)
-- {
--   f(0)
-- }
-- else if (d.sum_lf < 2)
-- {
--   f(1)
-- }
-- ...
-- else
-- {
--   f(n-1)
-- }
gen_ifSeries :: Int -> (Int -> [String]) -> [String]
gen_ifSeries n f = concat $ map subgenerate [0..n-1]
	where
		subgenerate 0 = [
			"if (d.sum_lf < 1)",
			"{"
			] ++ indent (f 0) ++
			["}"]
		subgenerate i = [
			"else if (d.sum_lf < " ++ (show $ 1 + i) ++ ")",
			"{"
			] ++ indent (f i) ++
			["}"]

gen_noise :: Int -> [String]
gen_noise n = [
		"template <> typename NoiseOpenSimplex<" ++ (show n) ++ ">::Data",
		"NoiseOpenSimplex<" ++ (show n) ++ ">::at(real const x[" ++ (show n) ++ "]) const",
		"{"
	] ++
	-- Constants
	[
		"\tconstexpr auto n = " ++ (show n) ++ ";",
		"\tconstexpr real EUCLIDEAN = " ++ (show $ sigma_euclidean n) ++ ";",
		"\tconstexpr real LATTICE = " ++ (show $ sigma_lattice n) ++ ";"
	] ++ (map euclidean_multipler [1..n]) ++
	-- Function body
	[
		"\tData d;",
		"\treal const lattice_shift = (" ++ (gen_binOp "+" (gen_index "x") n) ++ ") * LATTICE;",
		"\treal const l[] = {"
	] ++ (map xl_lattice [0..n-1]) ++
	["\t};"] ++
	(map xl_latticeCorner [0..n-1]) ++
	["\tinteger const lj[] = {"] ++
	(map xl_latticeNextCorner [0..n-1]) ++
	["\t};"] ++
	(map xl_latticeRelative [0..n-1]) ++
	[
		"\treal const euclidean_shift = (" ++ (gen_binOp "+" (gen_index "d.li") n) ++ ") * EUCLIDEAN;",
		"\treal const ei[] = {"
	] ++
	(map xl_euclideanCorner [0..n-1]) ++
	["\t};"] ++
	(map xl_euclideanRelative [0..n-1]) ++
	[
		"",
		"\td.sum_lf = " ++ (gen_binOp "+" (gen_index "d.lf") n) ++ ";",
		"\td.f = 0;"
	] ++ (indent $ gen_ifSeries n gen_simplex) ++
	-- Add contributions from each vertex
	-- Function tail
	[
		"\treturn d;",
		"}"
	]
	where
		euclidean_multipler i = "\tconstexpr real EUCLIDEAN_" ++ (show i) ++ " = EUCLIDEAN * " ++ (show i) ++ ";"
		x_index i = "x[" ++ (show i) ++ "]"
		xl_lattice i = "\t\t" ++ (gen_index "x" i) ++ " + " ++ "lattice_shift,"
		xl_latticeCorner i = "\t" ++ (gen_index "d.li" i) ++ " = " ++
			"floor_fast(" ++ (gen_index "l" i) ++ ");"
		xl_latticeNextCorner i = "\t\t" ++ (gen_index "d.li" i) ++ " + 1,"
		xl_latticeRelative i = "\t" ++ (gen_index "d.lf" i) ++ " = " ++
			(gen_index "l" i) ++ " - " ++ (gen_index "d.li" i) ++ ";"
		xl_euclideanCorner i = "\t\t" ++ (gen_index "d.li" i) ++ " + euclidean_shift,"
		xl_euclideanRelative i = "\t" ++ (gen_index "d.ef" i) ++ " = " ++
			(gen_index "x" i) ++ " - " ++ (gen_index "ei" i) ++ ";"
		gen_simplex m = ["d.simplex = " ++ (show m) ++ ";"] ++
			(gen_hyperplane n m) ++ (gen_hyperplane n $ m + 1)


-- | Generate everything corresponding to a dimension
gen_instantiate :: Int -> [String]
gen_instantiate n = ["template class NoiseOpenSimplex<" ++ (show n) ++ ">;"]

gen_all :: Int -> String
gen_all n = concat $ map (++ "\n") li
	where
		li = (gen_extrapolate n) ++ (gen_noise n) ++ (gen_instantiate n)


test_all = List.foldl' (&&) True [
		assertEqual ((combinatorial 3 2) == [[0,1,1],[1,0,1],[1,1,0]])
	]
	where
		assertEqual :: Bool -> Bool
		assertEqual x = CEB.assert x True

main = do
	putStrLn $ if test_all then "// All code gen tests passed" else "// WARNING: Test failed"
	putStrLn $ concat $ map gen_all dim
