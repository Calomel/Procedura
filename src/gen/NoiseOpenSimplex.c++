#include "gen/NoiseOpenSimplex.h++"

#include <algorithm>
#include <random>

namespace
{
	constexpr int PERM_SIZE = 8192;
	constexpr int PERM_MASK = PERM_SIZE - 1;

	// +4 to accomodate for extra dimensions
	constexpr int SURFLET_SIZE = PERM_SIZE + 4;

} // namespace <anonymous>

template <int n>
NoiseOpenSimplex<n>::NoiseOpenSimplex(int seed)
{
	std::default_random_engine generator(seed);
	std::uniform_int_distribution<int> distribution_perm(0, PERM_MASK);
	std::uniform_real_distribution<real> distribution_surf(-1.0, 1.0);

	surflets_ = new real[SURFLET_SIZE];
	for (int i = 0; i < SURFLET_SIZE; ++i)
		surflets_[i] = distribution_surf(generator);

	for (int i = 0; i < n; ++i)
	{
		perm_[i] = new int[PERM_SIZE];
		for (int j = 0; j < PERM_SIZE; ++j)
			perm_[i][j] = j;

		// Fisher-Yates shuffle on the permutation
		std::shuffle(perm_[i], perm_[i] + PERM_SIZE, generator);
	}
	
}
template <int n>
NoiseOpenSimplex<n>::~NoiseOpenSimplex()
{
	delete surflets_;
	for (int i = 0; i < n; ++i)
		delete perm_[i];
}

namespace
{
	inline real pow2(real x)
	{
		return x * x;
	}
	inline real pow3(real x)
	{
		return x * x * x;
	}
	inline real pow4(real x)
	{
		x *= x;
		return x * x;
	}

} // namespace

///////////////////////////
// Generated Code
///////////////////////////
