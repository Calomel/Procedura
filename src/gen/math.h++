#pragma once

#include <cstdint>

typedef double real;
typedef std::int64_t integer;

/**
 * Floor gives false results on negative integers.
 * Ceil gives false results on positive integers.
 */
inline integer
floor_fast(real const in)
{
	return in < 0 ? integer(in) - 1 : integer(in);
}
inline integer
ceil_fast(real const in)
{
	return in > 0 ? integer(in) + 1 : integer(in);
}
