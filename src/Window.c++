#include "Window.h++"

#include <iostream>

#include <GLFW/glfw3.h>

#include "util.h++"

bool
assertGLNoError()
{
#ifndef NDEBUG
	{
		switch(glGetError())
		{
		case GL_NO_ERROR: return true;
		case GL_INVALID_ENUM:
			assert(false, "GL Invalid Enum");
			return false;
		case GL_INVALID_VALUE:
			assert(false, "GL Invalid Value");
			return false;
		case GL_INVALID_OPERATION:
			assert(false, "GL Invalid Operation");
			return false;
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			assert(false, "GL Invalid Frame Buffer Operation");
			return false;
			break;
		case GL_OUT_OF_MEMORY:
			assert(false, "GL Out of memory");
			return false;
		default:
			assert(false, "GL Other errors");
			return false;
		};
	}
#endif
	assertUnreachable();
	return true;
}

namespace
{

	constexpr GLuint GL_FAIL = 0;

	/**
	 * @brief Register a GL shader of given type.
	 * @return Non-zero if successful.
	 */
	GLuint
	registerGLShader(char const source[], GLenum type) noexcept
	{
		// Compile the shader from source
		GLuint shader = glCreateShader(type);
		if (!shader)
			return GL_FAIL;
		glShaderSource(shader, 1, &source, nullptr);
		glCompileShader(shader);
	
		// If error occurs, return false
	
		GLint result = GL_FALSE;
		int infoLogLength;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			char* const error = new char[infoLogLength + 1];
			glGetShaderInfoLog(shader, infoLogLength, nullptr, error);
			std::cerr << error << std::endl;
			delete[] error;
	  	glDeleteShader(shader);
			return GL_FAIL;
		}
		
		return shader;
	}

} // namespace <anonymous>

GLuint
registerGLProgram(char const sourceVert[], char const sourceFrag[])
{
	assert(sourceVert && sourceFrag, "Invalid source");

	// Compile the shaders
	GLuint const shaderVert = registerGLShader(sourceVert, GL_VERTEX_SHADER);
	if (!shaderVert)
	{
		assertFail("GL Fragment Shader register");
		return GL_FAIL;
	}
	GLuint const shaderFrag = registerGLShader(sourceFrag, GL_FRAGMENT_SHADER);
	if (!shaderFrag)
	{
		assertFail("GL Fragment Shader register");
		return GL_FAIL;
	}

	// Register the program
	GLuint const program = glCreateProgram();
	glAttachShader(program, shaderVert);
	glAttachShader(program, shaderFrag);
	glLinkProgram(program);

	// If error occurs, return false
	GLint result = GL_FALSE;
	int infoLogLength;
	glGetProgramiv(program, GL_LINK_STATUS, &result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0)
	{
		char* error = new char[infoLogLength + 1];
		glGetProgramInfoLog(program, infoLogLength, nullptr, error);
		std::cerr << error << std::endl;
		delete[] error;
		assertFail("GL Program register");
		return GL_FAIL;
	}

	// Clean up
	glDetachShader(program, shaderVert);
	glDetachShader(program, shaderFrag);
	glDeleteShader(shaderVert);
	glDeleteShader(shaderFrag);

	return program;
}


// Static assert that {@code enum class Key} agrees with GLFW
// Only the beginning and ending of contiguous blocks are checked.
#define ENUM_KEY_CHECK(entry, glfwName) \
	static_assert( (int) Key::entry == GLFW_KEY_##glfwName, \
	               "enum class Key does not match GLFW: Key::" #entry " != " \
	               "GLFW_KEY_"#glfwName)
ENUM_KEY_CHECK(Unknown, UNKNOWN);
ENUM_KEY_CHECK(Space, SPACE);
ENUM_KEY_CHECK(Apostrophe, APOSTROPHE);
ENUM_KEY_CHECK(Comma, COMMA);
ENUM_KEY_CHECK(Slash, SLASH);
ENUM_KEY_CHECK(N_0, 0);
ENUM_KEY_CHECK(N_9, 9);
ENUM_KEY_CHECK(Semicolon, SEMICOLON);
ENUM_KEY_CHECK(Equal, EQUAL);
ENUM_KEY_CHECK(A, A);
ENUM_KEY_CHECK(Z, Z);
ENUM_KEY_CHECK(BracketLeft, LEFT_BRACKET);
ENUM_KEY_CHECK(BracketRight, RIGHT_BRACKET);
ENUM_KEY_CHECK(Grave, GRAVE_ACCENT);
ENUM_KEY_CHECK(World1, WORLD_1);
ENUM_KEY_CHECK(World2, WORLD_2);
ENUM_KEY_CHECK(Escape, ESCAPE);
ENUM_KEY_CHECK(End, END);
ENUM_KEY_CHECK(CapsLock, CAPS_LOCK);
ENUM_KEY_CHECK(Pause, PAUSE);
ENUM_KEY_CHECK(F1, F1);
ENUM_KEY_CHECK(F25, F25);
ENUM_KEY_CHECK(KP_0, KP_0);
ENUM_KEY_CHECK(KP_Equal, KP_EQUAL);
ENUM_KEY_CHECK(ShiftLeft, LEFT_SHIFT);
ENUM_KEY_CHECK(Menu, MENU);
#undef ENUM_KEY_CHECK


// Static assert that {@code enum class KeyMod} agrees with GLFW
#define ENUM_KEYMOD_CHECK(entry, glfwName) \
	static_assert( (int) KeyMod::entry == GLFW_MOD_##glfwName, \
	               "enum class KeyMod does not match GLFW: KeyMod::" #entry " != " \
	               "GLFW_MOD_"#glfwName)
ENUM_KEYMOD_CHECK(Shift, SHIFT);
ENUM_KEYMOD_CHECK(Control, CONTROL);
ENUM_KEYMOD_CHECK(Alt, ALT);
ENUM_KEYMOD_CHECK(Super, SUPER);
#undef ENUM_KEYMOD_CHECK

#define ENUM_KEYACTION_CHECK(entry, glfwName) \
	static_assert( (int) KeyAction::entry == GLFW_##glfwName, \
	               "enum class KeyAction does not match GLFW: KeyAction::" #entry " != " \
	               "GLFW_"#glfwName)
ENUM_KEYACTION_CHECK(Release, RELEASE);
ENUM_KEYACTION_CHECK(Press, PRESS);
ENUM_KEYACTION_CHECK(Repeat, REPEAT);
#undef ENUM_KEYACTION_CHECK


// Class Window

void
Window::init()
{
	bool flag = glfwInit();
	assert(flag, "GLFW Initialisation");
}

#ifndef NDEBUG
void
debug_callback(GLenum source,
               GLenum type,
               GLuint id,
               GLenum severity,
               GLsizei length,
               const GLchar* message,
               const void* userParam)
{
	std::cerr << "GL";
	if (type == GL_DEBUG_TYPE_ERROR)
		std::cerr << " Error";
	std::cerr << ": type = " << type
	          << ", severity: " << severity
	          << ", message: " << message
	          << "\n";
}
#endif

Window*
Window::create(int width, int height)
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	GLFWwindow* const w = glfwCreateWindow(width, height, "Procedura",
	                                       nullptr, nullptr);
	glfwSetKeyCallback(w, +[](GLFWwindow* window,
		int key, int scancode, int action, int mods)
	{
		auto& actions = windowMap[window]->actions_;
		if (actions.size() < 256)
			actions.push(Action { (Key) key, (KeyAction) action, (KeyMod) mods });
	});


	assert(w, "GLFW window creation");

	Window* const result = new Window(w);
	result->width_ = width;
	result->height_ = height;

	windowMap[w] = result;

	return result;
}

void
Window::activate()
{
	glfwMakeContextCurrent(binding_);

	// GL functions are available after this poinnt

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
		assertFail("GLEW initialisation");
	glViewport(0, 0, width_, height_);

	// Reset the time stamp
	timeStamp_ = std::chrono::high_resolution_clock::now();
	frameDuration_ = 1.0 / 30.0;

#ifndef NDEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(debug_callback, 0);
#endif

}
bool
Window::loop()
{
	glfwSwapBuffers(binding_);
	glfwPollEvents();

	{
		using namespace std::chrono;
		auto const timeStamp2 = high_resolution_clock::now();
		frameDuration_ = duration_cast<microseconds>(timeStamp2 - timeStamp_).count() *
			0.000001;
		timeStamp_ = timeStamp2;
	}
	return !glfwWindowShouldClose(binding_);
}
KeyAction
Window::getKeyAction(Key k) const
{
	return (KeyAction) glfwGetKey(binding_, (int) k);
}

std::unordered_map<GLFWwindow*, Window*> Window::windowMap;

Window::Window(GLFWwindow* const w):
	binding_(w)
{
	assert(w, "Invalid window");
}

Window::~Window()
{
	glfwDestroyWindow(binding_);
	windowMap.erase(windowMap.find(binding_));
}
