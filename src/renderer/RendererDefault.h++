#pragma once

#include "Renderer.h++"

/**
 * Renders the Gaussian texture.
 *
 * Navigation: Arrow keys to move, - and = keys to zoom.
 */
class RendererDefault final: public Renderer
{
public:
	RendererDefault(Window* w): Renderer(w),
		texture_(nullptr), data_(nullptr)
	{
	}
	~RendererDefault();

protected:
	virtual void init() override;
	void reset();
	virtual void* texture() override;

private:
	float* texture_;
	float* data_;

	double x_, y_, scale_;

	/**
	 * If dirty is true, the entire texture needs to be recalculated
	 */
	bool dirty_;
};
