#pragma once

#include "Renderer.h++"
#include "../gen/NoiseOpenSimplex.h++"

/**
 * Renders the OpenSimplex noise texture.
 *
 * Navigation: Arrow keys to move, - and = keys to zoom.
 * Modes:
 *       Q -> Simplex mode
 *       W -> Simplex central axis mode
 *       A -> Result mode
 *       S -> Lattice converage mode, blending simplex and result
 *
 *       Z -> Reset
 */
class RendererOpenSimplex final: public Renderer
{
public:
	RendererOpenSimplex(Window* w): Renderer(w),
		texture_(nullptr), data2(nullptr)
	{
	}
	~RendererOpenSimplex();

protected:
	virtual void init() override;
	void reset();
	virtual void* texture() override;

private:
	enum class Mode
	{
		Simplex,
		CentralAxis,
		Output,
		OutputWithLattice,
	};

	float* texture_;

	double x_, y_, scale_;
	double min_, max_;
	Mode mode_;

	/**
	 * If dirtyMode is true, the mode has changed but the noise doesn't need to
	 * be recalculated.
	 *
	 * If dirty is true, the entire texture needs to be recalculated
	 */
	bool dirtyMode_, dirty_;

	NoiseOpenSimplex<2>::Data* data2;
	NoiseOpenSimplex<2> noise2;
};
