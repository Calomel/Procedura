#include "Renderer.h++"

#include <GL/glew.h>

#include "../util.h++"
#include "../Window.h++"

namespace
{
	/**
	 * A rectangle from (-1,-1) to (1,1)
	 */
	GLfloat const quad_vertices[] =
	{
		// X  Y  Z  U  V
		-1.0f, -1.0f, 0.0f,  0.0f, 0.0f,
		1.0f, -1.0f, 0.0f,   1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,   0.0f, 1.0f,
		-1.0f, 1.0f, 0.0f,   0.0f, 1.0f,
		1.0f, -1.0f, 0.0f,   1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,    1.0f, 1.0f,
	};
	char const sourceVert[] =
	  "#version 330 core\n"
	  "layout(location = 0) in vec3 pos;"
	  "layout(location = 1) in vec2 uv_in;"
	  "out vec2 uv;"
	  "void main()"
	  "{"
	  "    gl_Position = vec4(pos, 1);"
	  "    uv = uv_in;"
	  "}";
	char const sourceFrag[] =
	  "#version 330 core\n"
	  "in vec2 uv;"
	  "uniform sampler2D sampler;"
	  "out vec4 color;"
	  "void main()"
	  "{"
	  "    color = texture(sampler, uv);"
	  "}";

} // namespace <anonymous>

struct RendererData
{
	GLuint vertexArray;
	GLuint vertexBuffer;
	GLuint program;
	GLuint attrib_pos;
	GLuint attrib_uvin;
	GLuint texture;
};

static_assert(sizeof(float) == sizeof(GLfloat), "Unequal floats");

Renderer::Renderer(Window* w):
	window_(w),
	data_(new RendererData)
{
	glGenVertexArrays(1, &data_->vertexArray);
	glBindVertexArray(data_->vertexArray);

	glGenBuffers(1, &data_->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, data_->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER,
			sizeof(quad_vertices),
			quad_vertices,
			GL_DYNAMIC_DRAW);

	data_->program = registerGLProgram(sourceVert, sourceFrag);
	assert(data_->program, "GL Program");
	data_->attrib_pos = glGetAttribLocation(data_->program, "pos");
	assert(data_->attrib_pos != -1, "Invalid attribute");
	data_->attrib_uvin = glGetAttribLocation(data_->program, "uv_in");
	assert(data_->attrib_pos != -1, "Invalid attribute");

#ifndef NDEBUG
	std::cout << "Bound attributes: [" << data_->attrib_pos << "]=pos, "
	          << "[" << data_->attrib_uvin << "]=uvin\n";
#endif

	assertGLNoError();

	glGenTextures(1, &data_->texture);
	glBindTexture(GL_TEXTURE_2D, data_->texture);
	glTextureStorage2D(data_->texture, 1, GL_RGBA32F,
			window_->width(),
			window_->height());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	assertGLNoError();
}

Renderer::~Renderer()
{
	delete data_;
}

void Renderer::start()
{
	init();

	assertGLNoError();

	RendererData& d = *data_;

	int const width = window_->width();
	int const height = window_->height();

	while (window_->loop())
	{
		glClearColor(0.0f, 0.2f, 0.2f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


		glBindTexture(GL_TEXTURE_2D, d.texture);

		assert(assertGLNoError(), "GL Error");

		void* const pixels = texture();
		if (pixels)
		{
			glTextureSubImage2D(d.texture, // Target
					0, // Mipmap
					0, 0, width, height, // X offset, Y o ffset, Width, height
					GL_RGBA, // Format
					GL_FLOAT,
					pixels);
		}

		glActiveTexture(GL_TEXTURE0);

		glUseProgram(d.program);

		assert(assertGLNoError(), "GL Error");
		glEnableVertexAttribArray(d.attrib_pos);
		glEnableVertexAttribArray(d.attrib_uvin);
		glBindBuffer(GL_ARRAY_BUFFER, d.vertexBuffer);
		glVertexAttribPointer(
		  d.attrib_pos,     // Shader parameter Id
		  3,        // Each vertex position has 3 dimensions
		  GL_FLOAT, // Value type
		  GL_FALSE, // Normalised?
		  5 * sizeof(GLfloat),        // Stride
		  0  // Array buffer offset
		);
		glVertexAttribPointer(
		  d.attrib_uvin,    // Shader parameter Id
		  2,        // Each uv position has 2 dimensions
		  GL_FLOAT, // Value type
		  GL_FALSE, // Normalised?
		  5 * sizeof(GLfloat),        // Stride
		  (const GLvoid*) (3 * sizeof(GLfloat)) // Array buffer offset
		);

		glDrawArrays(GL_TRIANGLES, 0, sizeof(quad_vertices) / sizeof(quad_vertices[0]));
		glDisableVertexAttribArray(d.attrib_pos);
		glDisableVertexAttribArray(d.attrib_uvin);

		assert(assertGLNoError(), "GL Error");
	}
	assertGLNoError();
}
